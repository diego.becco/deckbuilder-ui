# Kata - Deck Manager in Unity

The goal of this exercise is to master most of the vital UI components in Unity Game Engine.

## Rules of engagement
* The resolution set for this project will be 1920x1080p
* The final product must support Portrait AND Landscape orientations
* All scrollable elements must implement a mask
* The project contains sprites for this kata.
* The project contains some helpfull scripts.
* This exercise was designed for Unity 2018

## Part 1 - Card Popup
* Create a popup panel that shows upon clicking on a card from the bank.
* The popup must display all of the card data as shown in the [reference image](https://drive.google.com/file/d/13BDyZ7FlC311aisfkafdItowF1RAVD1h/view?usp=sharing):
> * Cost
> * Name
> * Art
> * Description
> * Type and Rarity (Rarity is desplayed by changing the type icon color)
> * Attack
> * Defense

* The popup must also contain a button to close it.

## Part 2 (optional) - Card Bank
* Create a panel where all cards are listed as simple rows.
* All items must contain their Cost and Name as seen in the [reference image](https://drive.google.com/file/d/1GbeInC8aJXrUKfQGDqbotsCQc-aU6ruX/view?usp=sharing).
* The contents of the panel must be visible by vertical scroll functionality.
* The project contains scripts to handle card data and its generation. USE THEM!


## Callistenics:
* Gameobject component cannot modify states outside its own Hierarchy. Example: DeckInfoPanel.TotalCards.text = 5;
* The scene must be responsive and be viewable both in Landscape and Portrait
